export const IS_LOGGED_IN = () => {
    const token = localStorage.getItem('token')
    if( !token || token === '' ){
        localStorage.clear();
        return false;
    }else{
        return true;
    }
}