import React, { useState } from 'react'
import { 
  Grid,
  Link,
  TextField,
  CssBaseline,
  Button,
  Avatar,
  Typography,
  Container,
  CircularProgress,
} from '@material-ui/core';
import Alert from '@material-ui/lab/Alert';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';

import { makeStyles } from '@material-ui/core/styles';

import { signIn } from '../services/users';
import { useRouter } from 'next/router';


const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(8),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: '100%',
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
  MuiFooter: {
    margin: '12px auto 0 auto ',
    justifyItems: 'center',
  },
  MuiCircularProgress: {
    position: 'absolute',
    zIndex: 99,
  }
}));

export default function SignIn() {
  const classes = useStyles();
  const router = useRouter();

  const [email, setEmail] = useState('josedavid.bruges@gmail.com');
  const [password, setPassword] = useState('123456');
  const [validEmail, setValidEmail] = useState(true);
  const [validPass, setValidPass] = useState(true);
  const [loading, setLoading] = useState(false);
  const [errorLabel, setErrorLabel] = useState(null);
  const [bottomErrorClose, setBottomErrorClose] = useState(false);

  const validateEmail = event => {
    setEmail(event.target.value)
    const regExr = /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i
    if (regExr.test(event.target.value)){
      setValidEmail(true);
     } else {
      setValidEmail(false);
     }
  }
  const validatePassword = event => {
    setPassword(event.target.value)
    if ( event.target.value.length > 3) {
      setValidPass(true);
    } else {
      setValidPass(false);
    }
  }

  const onLoginAuth = e => {
    e.preventDefault()
    setLoading(true);
    setErrorLabel('');
    setBottomErrorClose(false);
    signIn({ email, password }).then(result => {
      setLoading(false);
      localStorage.setItem('token', result?.token);
      localStorage.setItem('currentUser', JSON.stringify(result?.userAuth));
      router.push('/dashboard');
    }, error => {
      if ( error?.response?.data ) {
        setErrorLabel(error?.response?.data?.message);
        setBottomErrorClose(true);
      }
      setLoading(false);
    })
  }

  return (
    <Container component="main">
      <CssBaseline />
      {bottomErrorClose && <Alert severity="error" onClose={() => {setBottomErrorClose(!bottomErrorClose)}}>
        {errorLabel}
      </Alert>}

      <div className={classes.paper}>
        <Avatar className={classes.avatar}>
          <LockOutlinedIcon />
        </Avatar>
        <Typography component="h1" variant="h5">
          Sign in
        </Typography>
        <form className={classes.form} noValidate>
          <TextField
            id="email"
            type="email"
            name="email"
            inputProps={{ 'aria-label': 'email' }}
            defaultValue={email}
            required
            label="Email Address"
            variant="outlined"
            margin="normal"
            fullWidth
            autoComplete="email"
            autoFocus
            onChange={validateEmail}
          />
          <TextField
            id="password"
            type="password"
            name="password"
            variant="outlined"
            defaultValue={password}
            margin="normal"
            required
            fullWidth
            label="Password"
            autoComplete="current-password"
            onChange={validatePassword}
          />
          <Button
            fullWidth
            variant="contained"
            color="primary"
            className={classes.submit}
            disabled={!validEmail || !validPass || loading}
            onClick={onLoginAuth}
          >
            Sign In
            {loading && <CircularProgress size={24} color="primary" className={classes.MuiCircularProgress} />}
          </Button>
          <Grid container>
            <Grid item className={classes.MuiFooter}>
              <Link href="/signup" variant="body2">
                Don't have an account? Sign Up
              </Link>
            </Grid>
          </Grid>
        </form>
      </div>
    </Container>
  );
}