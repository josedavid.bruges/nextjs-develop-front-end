import React, { useState, useEffect } from 'react'
import { makeStyles } from '@material-ui/core/styles';
import {
  CssBaseline,
  List,
  ListItem,
  ListItemIcon,
  ListItemText,
  Divider,
  Avatar,
  Typography,
  Fab,
  Link,
} from '@material-ui/core';

import AccountCircleIcon from '@material-ui/icons/AccountCircle';
import AddIcon from '@material-ui/icons/Add';

import {
  Alert,
  Skeleton,
} from '@material-ui/lab';


import { usersList } from '../services/users';
import { IS_LOGGED_IN } from '../utils/auth'

import { useRouter } from 'next/router';

const useStyles = makeStyles((theme) => ({
    root: {
      width: '100%',
    },
    MuiFab: {
      position: 'fixed',
      bottom: 10,
      right: 10,
    }
}));

export default function Dashboard() {
    const classes = useStyles();
    const router = useRouter();

    const [errorLabel, setErrorLabel] = useState(null);
    const [bottomErrorClose, setBottomErrorClose] = useState(false);
    const [loading, setLoading] = useState(false);
    const [users, setUsers] = useState([]);

    useEffect(() => {
      if (!IS_LOGGED_IN()) {
        router.push('/');
      }else{
        setLoading(true);
        usersList().then(result => {
          console.log('\n\n\n\n\n result?.data: ', result?.data, '\n\n\n\n\n')
          setUsers(result?.data)
          setLoading(false);
        }, error => {
          if ( error?.response?.data ) {
            setErrorLabel(error?.response?.data?.message);
            setBottomErrorClose(true);
          }
          setLoading(false);
        })
      }
    }, [])

    const onToRegister = () => {
      router.push('/signup');
    }
  
    return (
      <div className={classes.root}>

        <CssBaseline />

        {bottomErrorClose && <Alert className={classes.MuiAlert} severity="error" onClose={() => {setBottomErrorClose(!bottomErrorClose)}}>
          {errorLabel}
        </Alert>}

        {loading && users.length === 0 && (
          <List component="nav" aria-label="main mailbox folders">
            <ListItem button>
              <ListItemIcon>
                <Skeleton variant="circle">
                  <Avatar />
                </Skeleton>
              </ListItemIcon>
              <Skeleton width="100%">
                <Typography>.</Typography>
              </Skeleton>
            </ListItem>
            <Divider />
            <ListItem button>
              <ListItemIcon>
                <Skeleton variant="circle">
                  <Avatar />
                </Skeleton>
              </ListItemIcon>
              <Skeleton width="100%">
                <Typography>.</Typography>
              </Skeleton>
            </ListItem>
            <Divider />
            <ListItem button>
              <ListItemIcon>
                <Skeleton variant="circle">
                  <Avatar />
                </Skeleton>
              </ListItemIcon>
              <Skeleton width="100%">
                <Typography>.</Typography>
              </Skeleton>
            </ListItem>
            <Divider />
            <ListItem button>
              <ListItemIcon>
                <Skeleton variant="circle">
                  <Avatar />
                </Skeleton>
              </ListItemIcon>
              <Skeleton width="100%">
                <Typography>.</Typography>
              </Skeleton>
            </ListItem>
            <Divider />
            <ListItem button>
              <ListItemIcon>
                <Skeleton variant="circle">
                  <Avatar />
                </Skeleton>
              </ListItemIcon>
              <Skeleton width="100%">
                <Typography>.</Typography>
              </Skeleton>
            </ListItem>
            <Divider />
          </List>
        )}

        <List component="nav" aria-label="main mailbox folders">
          {users.map((item) => (
            <>
              <ListItem button>
                <ListItemIcon>
                  <AccountCircleIcon />
                </ListItemIcon>
                <ListItemText primary={item?.email} />
              </ListItem>
              <Divider />
            </>
          ))}
        </List>

        <Fab
          className={classes.MuiFab}
          color="primary"
          aria-label="add"
          onClick={onToRegister}
        >
          <AddIcon />
        </Fab>
      </div>
    );
  }