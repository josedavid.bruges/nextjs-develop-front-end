import React, { useState, useEffect } from 'react'
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import Link from '@material-ui/core/Link';
import Grid from '@material-ui/core/Grid';
import CircularProgress from '@material-ui/core/CircularProgress';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';

import Alert from '@material-ui/lab/Alert';

import { signUp } from '../services/users';
import { useRouter } from 'next/router';

const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(8),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: '100%',
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
  MuiFooter: {
    marginTop: 10,
    justifyItems: 'center',
  },
  MuiCircularProgress: {
    position: 'absolute',
    zIndex: 99,
  }
}));

export default function SignUp() {
  const classes = useStyles();
  const router = useRouter();

  const [email, setEmail] = useState('josedavid.bruges@gmail.com');
  const [password, setPassword] = useState('123456');
  const [validEmail, setValidEmail] = useState(true);
  const [validPass, setValidPass] = useState(true);
  const [loading, setLoading] = useState(false);
  const [errorLabel, setErrorLabel] = useState(null);
  const [currentUser, setCurrentUser] = useState(null);
  const [bottomErrorClose, setBottomErrorClose] = useState(false);
  const [success, setSuccess] = useState(false);

  const validateEmail = event => {
    setEmail(event.target.value)
    const regExr = /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i
    if (regExr.test(event.target.value)){
      setValidEmail(true);
     } else {
      setValidEmail(false);
     }
  }
  const validatePassword = event => {
    setPassword(event.target.value)
    if ( event.target.value.length > 3) {
      setValidPass(true);
    } else {
      setValidPass(false);
    }
  }

  useEffect(() => {
    const currentUserTemp = localStorage.getItem('currentUser')
    if(currentUserTemp){
      setCurrentUser(JSON.parse(currentUserTemp) || null)
    }
  }, [])

  const onRegisterAuth = e => {
    e.preventDefault();
    let parentId = 0;
    if(currentUser){
      parentId = currentUser.id;
    }
    setLoading(true);
    setErrorLabel('');
    setBottomErrorClose(false);
    signUp({ email, password, parentId}).then(result => {
      setLoading(false);
      console.log('\n\n\n\n\n signUp | result: ', result, '\n\n\n\n\n\n')
      setSuccess(true)
    }, error => {
      if ( error?.response?.data ) {
        setErrorLabel(error?.response?.data?.message);
        setBottomErrorClose(true);
      }
      setLoading(false);
    })
  }

  const handlerSuccess = () => {
    if(!currentUser){
      router.push('/');
    }else{
      router.push('/dashboard');
    }
  }

  return (
    <Container component="main">
      <CssBaseline />
      {success && <Alert
        severity="success"
        onClose={() => {setBottomErrorClose(!bottomErrorClose)}}
        action={
          <Button
            color="inherit"
            size="small"
            onClick={handlerSuccess}
          >
            {currentUser? 'Dashboard' : 'Login'}
          </Button>
        }
      >
        Successfully registered user
      </Alert>}
      {bottomErrorClose && <Alert severity="error" onClose={() => {setBottomErrorClose(!bottomErrorClose)}}>
        {errorLabel}
      </Alert>}
      <div className={classes.paper}>
        <Avatar className={classes.avatar}>
          <LockOutlinedIcon />
        </Avatar>
        <Typography component="h1" variant="h5">
          Sign Up
        </Typography>
        <form className={classes.form} noValidate>
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            id="email"
            label="Email Address"
            name="email"
            autoComplete="email"
            autoFocus
            defaultValue={email}
            type="email"
            onChange={validateEmail}
          />
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            name="password"
            label="Password"
            type="password"
            id="password"
            autoComplete="current-password"
            defaultValue={password}
            onChange={validatePassword}
          />
          <Button
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
            className={classes.submit}
            disabled={!validEmail || !validPass || loading}
            onClick={onRegisterAuth}
          >
            Save
            {loading && <CircularProgress size={24} color="primary" className={classes.MuiCircularProgress} />}
          </Button>
          <Grid container>
            {!currentUser ? (
              <Grid item className={classes.MuiFooter}>
                <Link href="/" variant="body2">
                  Do you already have an account? login
                </Link>
              </Grid>
            ) : ''}
          </Grid>
        </form>
      </div>
    </Container>
  );
}