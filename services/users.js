const axios = require('axios')
axios.defaults.withCredentials = true;

const signInAxios = dataSendObj => {
  const url = `${process.env.MIDDLE_END_URL}/loginAuth`
  const headers = {
    'Content-Type': 'application/json',
  }
  const config = {
    method: 'POST',
    url: url,
    headers: headers,
    data: JSON.stringify(dataSendObj),
  }
  return new Promise((resolve, reject) => {
    axios(config)
      .then(function(response) {
        resolve(response?.data)
      })
      .catch(function(error) {
        console.log(error)
        reject(error)
      })
  })
}
export const signIn = async dataSendObj => {
  const res = await signInAxios(dataSendObj)
  return res
}

const signUpAxios = dataSendObj => {
  const url = `${process.env.MIDDLE_END_URL}/api/register`
  const headers = {
    'Content-Type': 'application/json',
  }
  const config = {
    method: 'POST',
    url: url,
    headers: headers,
    data: JSON.stringify(dataSendObj),
  }
  return new Promise((resolve, reject) => {
    axios(config)
      .then(function(response) {
        resolve(response?.data)
      })
      .catch(function(error) {
        console.log(error)
        reject(error)
      })
  })
}
export const signUp = async dataSendObj => {
  const res = await signUpAxios(dataSendObj)
  return res
}

const usersListAxios = token => {
  const url = `${process.env.MIDDLE_END_URL}/getListByParantId`
  const headers = {
    'Content-Type': 'application/json',
    'Authorization': `Bearer ${token}`,
  }
  const config = {
    method: 'POST',
    url: url,
    headers: headers,
    data: {},
  }
  return new Promise((resolve, reject) => {
    axios(config)
      .then(function(response) {
        resolve(response?.data)
      })
      .catch(function(error) {
        console.log(error)
        reject(error)
      })
  })
}
export const usersList = async () => {
  const token = localStorage.getItem('token')
  const res = await usersListAxios(token)
  return res
}