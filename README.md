This is a starter template for [Learn Next.js](https://nextjs.org/learn).

**Project React Front End: ** [https://gitlab.com/josedavid.bruges/nextjs-develop-front-end](https://gitlab.com/josedavid.bruges/nextjs-develop-front-end)

**Project Symfony Back End: ** [https://gitlab.com/josedavid.bruges/symfony-develop-back-end](https://gitlab.com/josedavid.bruges/symfony-develop-back-end)
